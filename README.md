# PDSI Chatter
Aplikacja zostala wykonan przy uzyciu technologii Angular 5, Node.js  
Link do boarda trello zawierajacego backlog: https://trello.com/b/prJHfw6M/pdsi-chatter  

# Dla czesci serwerowej:
npm install -g gulp-cli  
npm install  
gulp build  
npm start  
 
# Dla czesci klienckiej:
npm install  
ng serve

Kr�tki opis dzia�ania i funkcjonalno�ci aplikacji.  
Komunikator internetowy pozwalajacy dowolnej liczbie uzytkownikow rozmawiac, oraz dzielic sie plikami.
Uzytkownicy informowani sa o dolaczeniu nowych uzytkownikow do czatu.  
Kazdy uzytkownik ma mozliwosc zmiany swojej nazwy uzytkownika w trakcie dzialania aplikacji.   
Aplikacja wykorzystuje viewport i jest responsywna dla kazdego uzytkownika niezaleznie od posiadanego urzadzenia.  
Komunikacja w aplikacji odbywa sie poprzez sockety oraz klase HttpClient dostarczajaca metody do komunikacji z serwerem (REST API).  
Na chwile obecna dane przechowywane sa w localstorage, lecz docelowo beda przechowywane w bazie MongoDB, badz MySql.
W kwestii autoryzacji na stronie, na ten moment dostepne jest wylacznie uzytkowanie wspolnie tej samej aplikacji przez kilku uzytkownikow bez mozliwosci stalej rejestracji (brak podpiecia do bazy danych). 

Prezentacja:  
1. Prezentacja zespo�u i role, jakie poszczeg�lne osoby najcz�ciej przyjmowa�y w zespole. U�yte narz�dzia TeamWorku.  
Uzyte narzedzia podczas pracy to Trello zintegrowane z Bitbucketem na ktorym zostalo umieszczone repozytorium zawierajace kod, oraz assety.  
2. Funkcje aplikacji, kt�re zdecydowali�cie si� uwzgl�dni� w projekcie na tle opisu przedstawionego zadania, oraz jak wygl�da� proces selekcji. Czy w trakcie semestru co� si� zmieni�o?   
W trakcie trwania projektu natrafilem na kilka problemow w najnowszej wersji Angulara (Angular 5) z wykorzystaniem istniejacych rozwiazan dla wersji 2 oraz 4.  
Problemem, kt�rego sie nie spodziewalem bylo rowniez podpi�cie projektu i umieszczenie wszystkich danych do bazdy danych dynamicznie uzywajac formularza z Angulara.  
Na poczatku wybralem baze MongoDB, lecz koncowo nie zdazylem wykorzystac bazy danych MySQL i aplikacja pozosta�a bez bazy danych.  
3. Czym inspirowali�cie si� przy wyborze wygl�du aplikacji i u�ycia poszczeg�lnych gad�et�w  
Wybor technologii by� podyktowany preferencjami do technologii, kt�rych aktualnie u�ywam komercyjnie czyli Angular, Node (oraz React).  
Jesli chodzi o wyglad aplikacji chcialem wykorzystac vieport, poniewaz aplikacja sluzaca komunikacji nie potrzebuje specjalnych wodotrysk�w, a niezawodnosc i szybkosc dzialania.  
Funkcjonalnosci jakie chcialem wykorzystac sa uzywane przez uzytkownikow codziennie na wielu portalach, wiec podobne funkcjonalnosci chcialem wykorzystac w aplikacji.
4. Jakie �rodowiska programowania i uruchomienia zosta�y u�yte?  
Do stworzenia aplikacji zosta� wykorzystany framework Angular 5 (+ TypeScript) dla aplikacji klienckiej, oraz Node.js dla aplikacji serwerowej.    
W przyszlosci planuje dodac do aplikacji takze autoryzacje uzytkownikow przy uzyciu OAuth, oraz poprawnie zaimplementowac baze danych.  
Kod aplikacji jest tworzony na 3 systemach wymiennie (zalezy do jakiego komputera mam w danym momencie dostep), lecz najczesciej byl to OSX.  
Aplikacja dziala poprawnie na kazdym z tych systemow, oraz zostala poprawnie zbudowana na telefo z systemem Android.  
5. Opis technologii i konstrukcji technicznej z uwzgl�dnieniem niestandardowych algorytm�w lub po��cze� technologii (to centralna, najd�u�sza cz�� prezentacji).  
TBD personally :)
6. Wasze odczucia dotycz�ce tej formy zaj��, stopnia trudno�ci, ewentualna przysz�o�� projektu.  
Bardzo ciesze sie z takiej formy zajec, poniewaz w moim odczuciu napotykanie problemow w "wiekszym" projekcie daje duzo doswiadczenia, a z mojego doswiadczenia z pracy wiem, ze programista lepiej pamieta problem, ktory rozwiazywal kilka dnia, a nie ten, ktorego naprawienie zajelo kilka minut/godzin :)  
Tak jak juz wczesniej nadmienilem, jesli nadejdzie wolna chwila od pracy inzynierskiej oraz od pracy komercyjnej, bardzo chetnie dopracuje projekt i moze udostepnie kod publicznie na repozytorium.    